package com.hji.GeoPaper;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class GeoPaperService extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new GeoPaperEngine();
	}
	
	private class GeoPaperEngine extends Engine {
		
		private final Handler handler = new Handler();
		private final Runnable drawRunner = new Runnable() {
			
			public void run() {
				draw();
			}
		};
		private List<Circle> circles;
		private Paint paint = new Paint();
		private int width;
		private int height;
		private int maxNumber;
		private boolean visible = true;
		private boolean touchEnabled;
		
		public GeoPaperEngine() {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(GeoPaperService.this);
			maxNumber = Integer.valueOf(preferences.getString("number_of_circles", "4"));
			touchEnabled = preferences.getBoolean("touch", false);
			circles = new ArrayList<Circle>();
			paint.setAntiAlias(true);
			paint.setColor(Color.WHITE);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(10f);
			handler.post(drawRunner);
		}
		
		@Override
		public void onVisibilityChanged(boolean visible) {
			this.visible = visible;
			if (visible) {
				handler.post(drawRunner);
			} else {
				handler.removeCallbacks(drawRunner);
			}
		}
		
		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			
			this.visible = false;
			handler.removeCallbacks(drawRunner);
		}
		
		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			this.width = width;
			this.height = height;
			
			super.onSurfaceChanged(holder, format, width, height);
		}
		
		@Override
		public void onTouchEvent(MotionEvent event) {
			if (touchEnabled) {
				float x = event.getX();
				float y = event.getY();
				SurfaceHolder holder = getSurfaceHolder();
				Canvas canvas = null;
				
				try {
					canvas = holder.lockCanvas();
					if (canvas != null) {
						canvas.drawColor(Color.BLACK);
						circles.clear();
						circles.add(new Circle(String.valueOf(circles.size() + 1), x, y, (float) (Math.random() * 100)));
						drawCircles(canvas, circles);
					}
				} finally {
					if (canvas != null) {
						holder.unlockCanvasAndPost(canvas);
					}
				}
				super.onTouchEvent(event);
			}
		}
		
		private void draw() {
			SurfaceHolder holder = getSurfaceHolder();
			Canvas canvas = null;
			try {
				canvas = holder.lockCanvas();
				if (canvas != null) {
					if (circles.size() >= maxNumber) {
						circles.clear();
					}
					float x = (float) (width * Math.random());
					float y = (float) (height * Math.random());
					int randomAlpha = (int) (((x/width + y/height) / 2) * 255);
					Paint tempPaint = new Paint(paint);
					tempPaint.setAlpha(randomAlpha);
					circles.add(new Circle(String.valueOf(circles.size() + 1), x, y, (float) (Math.random() * 100), tempPaint));
					drawCircles(canvas, circles);
				}
			} finally {
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}
			handler.removeCallbacks(drawRunner);
			if (visible) {
				handler.postDelayed(drawRunner, 500);
			}
		}
		
		private void drawCircles(Canvas canvas, List<Circle> circles) {
			// Surface view requires everything be completely drawn.
			canvas.drawColor(Color.BLACK);
			
			for (Circle circle: circles) {
				canvas.drawCircle(circle.x, circle.y, circle.radius, paint);
			}
		}
	}
}
