package com.hji.GeoPaper;

import android.graphics.Paint;

public class Circle {

	String text;
	public float x;
	public float y;
	public float radius;
	public Paint paint;
	
	public Circle(String text, float x, float y) {
		this(text, x, y, 20f);
	}
	
	public Circle(String text, float x, float y, float radius) {
		this(text, x, y, radius, new Paint());
	}
	
	public Circle(String text, float x, float y, float radius, Paint paint) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.paint = paint;
	}
}
