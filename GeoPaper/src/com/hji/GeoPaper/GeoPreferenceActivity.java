package com.hji.GeoPaper;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class GeoPreferenceActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		// Only take numbers.
		Preference prefCircle = getPreferenceScreen().findPreference("number_of_circles");
		prefCircle.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				// Ensure we got a string.
				if (newValue != null && newValue.toString().length() > 0 && newValue.toString().matches("\\d*")) {
					return true;
				}
				// Otherwise, send an error message.
				Toast.makeText(GeoPreferenceActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
				return false;
			}
		});
	}
}
